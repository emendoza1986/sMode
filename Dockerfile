FROM node:13.3-alpine


RUN apk add --no-cache bash
RUN apk add --no-cache tini

# Set ENV variables
EXPOSE 3000
ENV PORT 3000
ENV APP_DIR app
ENV WEBSITE http://localhost:${PORT}
ENV NODE_ENV development


EXPOSE ${PORT}
WORKDIR /${APP_DIR}

ENV PUBLIC_DIR=/${APP_DIR}/public/
ENV SECURE_DIR=/${APP_DIR}/encrypted/



COPY package*.json ./

RUN npm install && npm cache clean --force

COPY . .

ENTRYPOINT ["tini", "--"]
CMD [ "node", "./bin/www" ]
