var express = require('express');

var QRCode = require('qrcode');

var csrf = require('csurf');

//middleware
var csrfMiddleware = csrf({ cookie: true });

var router = express.Router();

const __website = process.env.WEBSITE;
const __public =  process.env.PUBLIC_DIR;
const __encrypted = process.env.SECURE_DIR;



var SecretMode = require('../controller/secretMode');
SecretMode = new SecretMode(__website, __public, __encrypted);

/* GET INDEX page. */
router.get('/', csrfMiddleware, function(req, res, next) {
    console.error("================\nIndex URL\n");


    res.render('index', { website: __website, token: req.csrfToken() });
});

/* GET RETRIEVE DATA page. */
router.get('/decrypt/:iv_key_hmac_url_base64', csrfMiddleware, function(req, res, next) {
    console.error("================\nRetrieve URL\n");

    const isValid = SecretMode.retrieve(req.params.iv_key_hmac_url_base64);

    const inputs = SecretMode.inputNOTValidateSplits();

    if(isValid){
        res.render('retrieve', { website: __website, token: req.csrfToken(), decryption_url: req.params.iv_key_hmac_url_base64 });
    }else{
        res.render('error2', { website: __website});
    }
});

/* POST DECRYPTION DATA page. */
router.post('/decrypt/:iv_key_hmac_url_base64', csrfMiddleware, function(req, res, next) {
    console.error("================\nDecrypting Encrypted URL\n");

    var restoredText = SecretMode.decrypt(req.params.iv_key_hmac_url_base64);

    if(restoredText){
        res.render('decrypted', { website: __website, text: restoredText });
    }else{
        res.render('error2', { website: __website});
    }

});

/* POST ENCRYPTED DATA page */
router.post('/encrypt',csrfMiddleware, function(req, res, next) {
    console.error("================\nCreating Encrypted URL\n");

    var validation = true;
    var ttl, text;

    if(req.body.TTL)
        ttl = req.body.TTL;

    if( SecretMode.inputNOTValidateTTL(ttl) )
        validation = false;

    if(!req.body.note)
        validation = false;


    if(validation){
        text = req.body.note;
        const iv_key_hmac_url_base64 = SecretMode.encrypt(text,ttl);
        const url = __website + '/decrypt/'+iv_key_hmac_url_base64;
        //res.render('encrypted', { decryption_url: url, qrcode: qrcode });
        //QRCode.toDataURL(url, function (err, qrcode) {
        //    res.render('encrypted', { decryption_url: url, qrcode: qrcode });
        //})
        QRCode.toDataURL(url)
            .then(qrcode => {
                res.render('encrypted', { website: __website, decryption_url: url, qrcode: qrcode });
            })
            .catch(err => {
                console.error(err)
            })
    }else{
        res.redirect('/')
    }

});

/* JUNK URL REDIRECT. */
router.get('/:junk', function(req, res, next) {
    console.error("================\nJunk URL Redirect\n");

    res.redirect('/')
});


module.exports = router;

