
var fs = require("fs");
const path = require('path');
var secureRandom = require('secure-random');
var crypto = require('crypto');

const exec = require('child_process').exec;
var QRCode = require('qrcode');


const NodeCache = require( "node-cache" );

const encryptedFileCache = new NodeCache( { stdTTL: 300, checkperiod: 120 } );


class SecretMode {
    constructor(website, public_dir, encrypted_dir){
        this.__website = website;
        this.__public = public_dir;
        this.__encrypted = encrypted_dir;
    }

    encrypt (text, ttl) {
        //generate IV + KEY; convert to base64, unpadded, url safe.
        var iv, iv_base64_urlsafe;
        do{
            iv = secureRandom(16,{type: 'Buffer'});
            iv_base64_urlsafe = this.bytes2base64urlSafe(iv)
        }while(this.checkFileExists_sync(iv_base64_urlsafe));

        const key = secureRandom(32,{type: 'Buffer'});
        const key_base64_urlsafe = this.bytes2base64urlSafe(key);



        var cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
        var ciphertext = cipher.update(text, 'utf8','binary');
        ciphertext += cipher.final('binary');

        //Convert ciphertext + IV to base64
        const ciphertext_base64 = this.bytes2base64(ciphertext);
        const ciphertext_and_iv_base64 = ciphertext_base64 + iv_base64_urlsafe;

        //Calculate HMAC. Convert HMAC to base64, unpadded, url safe.
        /*
        var hmac = crypto.createHmac('sha224', key);
        hmac.update( ciphertext_and_iv_base64 );
        const hmac_base64_urlsafe = this.removeBase64Padding(hmac.digest('base64').replace(/\//g,'-'));
        */

        //debugging
        console.log('iv_base64: '+ iv_base64_urlsafe);
        console.log('key_base64: '+ key_base64_urlsafe);
        //console.log('hmac_base64: '+ hmac_base64_urlsafe);

        //Save Encrypted Data
        this.saveEncryptedData_async(ttl, iv_base64_urlsafe, ciphertext_base64);

        //Create and return Decryption URL
        //const iv_key_hmac_url_base64_urlsafe = ttl+'_'+iv_base64_urlsafe +'_'+key_base64_urlsafe+'_'+hmac_base64_urlsafe;
        const iv_key_hmac_url_base64_urlsafe = ttl+'_'+iv_base64_urlsafe +'_'+key_base64_urlsafe;

        return (iv_key_hmac_url_base64_urlsafe);
    };

    retrieve ( iv_key_hmac_url_base64_urlsafe ) {

        //Verify URL
        var res;
        res = this.inputNOTValidateURL(iv_key_hmac_url_base64_urlsafe);
        console.log('inputNOTValidateURL '+res);
        if(res === true)
            return false;

        var iv_key_hmac_base64_urlsafe = iv_key_hmac_url_base64_urlsafe.split('_');

        res =this.inputNOTValidateSplits(iv_key_hmac_base64_urlsafe.length);
        console.log('inputNOTValidateSplits '+res);
        if(res === true)
            return false;

        res = this.inputNOTValidateTTL(iv_key_hmac_base64_urlsafe[0]);
        if(res === true)
            return false;

        //res = this.inputNOTValidateLengths(iv_key_hmac_base64_urlsafe[1].length,iv_key_hmac_base64_urlsafe[2].length,iv_key_hmac_base64_urlsafe[3].length);
        res = this.inputNOTValidateLengths(iv_key_hmac_base64_urlsafe[1].length,iv_key_hmac_base64_urlsafe[2].length);
        console.log('inputNOTValidateLengths '+res);
        if(res === true)
            return false;

        const ttl = iv_key_hmac_base64_urlsafe[0];
        console.log('iv_base64: '+ iv_key_hmac_base64_urlsafe[1]);
        console.log('key_base64: '+ iv_key_hmac_base64_urlsafe[2]);
        //console.log('hmac_base64: '+ iv_key_hmac_base64_urlsafe[3]);

        const iv_base64_urlsafe = iv_key_hmac_base64_urlsafe[1];

        //Verify in Cache or Encrypted Data File Exists

        const filestatus = this.checkFileExists_sync(ttl,iv_base64_urlsafe);

        //Cache Encrypted File for Faster Decryption
        if(filestatus === true){
            fs.readFile(path.join(this.__encrypted,ttl,iv_base64_urlsafe), function (err, data) {
                if (err) {
                    return console.error('readfile err: '+err);
                }

                encryptedFileCache.set( iv_base64_urlsafe, data.toString(), function( err ){
                    if( err ){
                        console.log( 'file cache error' );
                    }else{
                        console.error('file successfully cached')
                    }
                });
                console.error('readFile Executed.')
            });
        }
        console.error(encryptedFileCache.getStats());
        return filestatus

    };

    decrypt ( iv_key_hmac_url_base64_urlsafe ) {

        //Verify URL
        var res;
        res = this.inputNOTValidateURL(iv_key_hmac_url_base64_urlsafe);
        console.log('inputNOTValidateURL '+res)

        var iv_key_hmac_base64_urlsafe = iv_key_hmac_url_base64_urlsafe.split('_');

        res =this.inputNOTValidateSplits(iv_key_hmac_base64_urlsafe.length);
        console.log('inputNOTValidateSplits '+res)
        if(res === true)
            return false;

        res = this.inputNOTValidateTTL(iv_key_hmac_base64_urlsafe[0]);
        if(res === true)
            return false;

        //res = this.inputNOTValidateLengths(iv_key_hmac_base64_urlsafe[1].length,iv_key_hmac_base64_urlsafe[2].length,iv_key_hmac_base64_urlsafe[3].length);
        res = this.inputNOTValidateLengths(iv_key_hmac_base64_urlsafe[1].length,iv_key_hmac_base64_urlsafe[2].length);
        console.log('inputNOTValidateLengths '+res)
        console.log('iv_base64: '+ iv_key_hmac_base64_urlsafe[1]);
        console.log('key_base64: '+ iv_key_hmac_base64_urlsafe[2]);
        //console.log('hmac_base64: '+ iv_key_hmac_base64_urlsafe[3]);
        if(res === true)
            return false;
        const ttl = iv_key_hmac_base64_urlsafe[0];

        const iv_base64_urlsafe = iv_key_hmac_base64_urlsafe[1];

        var iv_base64 = this.base64urlSafe2base64(iv_key_hmac_base64_urlsafe[1]);
        var key_base64 = this.base64urlSafe2base64(iv_key_hmac_base64_urlsafe[2]);
        //var hmac_base64 = this.base64urlSafe2base64(iv_key_hmac_base64_urlsafe[3]);
        console.log('iv_base64: '+ iv_base64);
        console.log('key_base64: '+ key_base64);
        //console.log('hmac_base64: '+ hmac_base64);

        const iv = this.base64toBytes(iv_base64);
        const key = this.base64toBytes(key_base64);
        console.log('iv.length: '+iv.length);
        console.log('key.length: '+key.length);
        //const hmac = this.base64toBytes(hmac_base64);

        var filestatus = false;
        try{
            console.error('accessing file via cache');
            var ciphertext_base64 = encryptedFileCache.get( iv_base64_urlsafe, true );
            filestatus = true;
            encryptedFileCache.del(iv_base64_urlsafe, function () {
                console.error('cached encrypted data DELETED.')
            });
        } catch( err ){
            console.error('accessing file directly');
            filestatus = this.checkFileExists_sync(ttl, iv_base64_urlsafe);
            if(filestatus === true){
                ciphertext_base64 = fs.readFileSync( path.join(this.__encrypted,ttl,iv_base64_urlsafe)).toString();
            }
        }

        //CHECK FILESTATUS BOOLEAN
        if(filestatus === true) {
            exec('rm ' + path.join(this.__encrypted,ttl,iv_base64_urlsafe), function (err) {
                if (err) {
                    console.error('delete err: ' + err);
                }
                console.log('successfully deleted ' + iv_base64_urlsafe);
            });


            console.error('ciphertext_length: ' + ciphertext_base64.length);

            try{
                var ciphertext = this.base64toBytes(ciphertext_base64);

                var decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
                var text = decipher.update(ciphertext, 'binary', 'utf8');
                text += decipher.final('utf8');
            } catch (err) {
                return false;
            }


            console.error(encryptedFileCache.getStats());
            return (text);

        }else{
            return false;
        }
    };

    bytes2base64 (bytes){
        return (Buffer.from( bytes ,'binary').toString('base64'));
    };

    bytes2hex (bytes){
        return (Buffer.from( bytes ,'binary').toString('hex'));
    };

    bytes2base64urlSafe (bytes){
        var base64custom = Buffer.from( bytes ,'binary').toString('base64');
        base64custom = this.removeBase64Padding(base64custom);
        return (base64custom.replace(/\//g,'-'))
    };
    base64urlSafe2bytes (base64urlSafe){
        var base64string = this.restoreBase64Padding(base64urlSafe.replace(/-/g,'/'))
        return (this.base64toBytes(base64string))
    };
    base64urlSafe2base64 (base64urlSafe){
        return (this.restoreBase64Padding(base64urlSafe.replace(/-/g,'/')))
    };

    base64toBytes (bytes){
        return (Buffer.from( bytes ,'base64'));
    };
    removeBase64Padding (base64_string){
        while( base64_string.charAt(base64_string.length -1) === '='){
            base64_string = base64_string.slice(0, -1);
        }
        return base64_string;
    };

    restoreBase64Padding (base64_string){
        while((base64_string.length % 4) !== 0){
            base64_string += '=';
        }
        return base64_string;
    };

    /* check synchronously if file exists */
    checkFileExists_sync (ttl, iv_base64_urlsafe){
        try {
            // file exists
            fs.statSync( path.join(this.__encrypted,ttl,iv_base64_urlsafe) ).isFile();
            return true;
        } catch (e) {
            // file does not exist
            return false;
        }


    };

    /* save encrypted data asynchronously */
    saveEncryptedData_async ( ttl, iv_base64_urlsafe , ciphertext_and_iv_base64 ){
        fs.writeFile( path.join(this.__encrypted,ttl,iv_base64_urlsafe), ciphertext_and_iv_base64, function(err) {
            if (err){
                console.warn('writeFile err: '+err);
            }
            console.log('The file has been saved!');
        });
    };

    inputNOTValidateURL ( URL ){
        const result = /[^A-Za-z0-9\-\+\_]/.test( URL );
        console.warn('url test: '+result);
        return result;
    };

    inputNOTValidateTTL ( TTL ){
        if( TTL.length !== 1)
            return true;

        const result = /[^1-4|7-8]/.test( TTL );
        console.warn('TTL test: '+result);
        return result;
    };
    inputNOTValidateSplits (split_size){
        return (split_size !== 3)
    };

    inputNOTValidateLengths (iv_length, key_length, hmac_length = 38){

        return (iv_length !== 22 || key_length !== 43 || hmac_length !== 38)
    };


}

module.exports = exports = SecretMode;